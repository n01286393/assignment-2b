﻿using System;
using System.Collections.Generic;
namespace oct
{
    public class Housing
    {
        //information we want in the Housing Object
        // unit
        // Street name
        // postal code
        // transportation
        // roommates 

        public List<string> transportation;
        private string streetNum;
        private string street;
        private string postalCode;
        private int roommateList;

        public Housing()
        {

        }

        public string StreetNum
        {
            get { return streetNum; }
            set { streetNum = value; }
        }
        public string Street
        {
            get { return street; }
            set { street = value; }
        }
        public string PostalCode
        {
            get { return postalCode; }
            set { postalCode = value; }
        }
        public int Roommates
        {
            get { return roommateList; }
            set { roommateList = value; }
        }
        public List<string> Transportation
        {
            get { return transportation; }
            set { transportation = value; }
        }
    }
}
