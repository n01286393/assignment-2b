﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace oct
{

    public partial class test123 : System.Web.UI.Page
    {

        //checking to see if user selected agree to terms and conditions 
        protected void TermsConditions_Validator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = termsConditions.SelectedValue == "Agree" ? true : false;
        }

        ////registration
        public void Registrations(object sender, EventArgs args)
        {

            //first checking to see if all the forms have been properly filled out and is valid
            if (!Page.IsValid)
            {
                return;
            }

            //creating the student object
            //collecting all the variables and setting the values.
            //setting the values later with a property accessor
            string fname = studentFirstName.Text.ToString();
            string lname = studentLastName.Text.ToString();
            string phone = studentPhoneNumber.Text.ToString();
            string num = studentNumber.Text.ToString();
            string email = studentEmail.Text.ToString();
            Student newstudent = new Student
            {
                StudentFirstName = fname,
                StudentLastName = lname,
                StudentPhoneNumber = phone,
                StudentNumber = num,
                StudentEmail = email
            };

            //creating the housing object
            //collecting all the variables and setting the values.
            //setting the values later with a property accessor
            string strnum = streetNum.Text.ToString();
            string str = street.Text.ToString();
            string postal = postalCode.Text.ToString();
            int roommates = int.Parse(roommateList.Text);
            List<String> transpo = new List<String> { "Methods of transportation are: " };
            Housing newhousing = new Housing
            {
                StreetNum = strnum,
                Street = str,
                PostalCode = postal,
                Roommates = roommates,
                Transportation = transpo
            };

            //creating the budget object
            //collecting all the variables and setting the values.
            //setting the values later with a property accessor
            double fcost = double.Parse(foodCost.Text);
            double tcost = double.Parse(transportationCost.Text);
            double ecost = double.Parse(entertainmentCost.Text);
            double bcost = double.Parse(boardingCost.Text);
            double mcost = double.Parse(miscCost.Text);
            Budget newbudget = new Budget
            {
                FoodCost = fcost,
                TransportationCost = tcost,
                EntertainmentCost = ecost,
                BoardingCost = bcost,
                MiscCost = mcost
            };

            //iterating over the transportation check list
            //based off of Christine's example
            List<string> housingtranspo = new List<string>();

            foreach (Control control in transpo_container.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    if (((CheckBox)control).Checked) //checks the checkbox, if checked add to the list 
                    {
                        housingtranspo.Add(((CheckBox)control).Text);
                    }

                }
            }
            newhousing.transportation = newhousing.transportation.Concat(housingtranspo).ToList();

            Registrations newregistrations = new Registrations(newstudent, newhousing, newbudget);

            StudentSum.InnerHtml = "Thank you, " + newstudent.StudentFirstName + " for your application!";

            Summary.InnerHtml = newregistrations.PrintSummary();

            //calculating the budget to see if they qualify for grant 
            GrantQualify.InnerHtml = newregistrations.CalculateGrant() == true
                ? "Congragulations! You qualify for the grant. We will contact you shortly with more information."
                : "Unfortunatly, you do not qualify for the Humber Grant. Please try again next semester.";

        }

    }
}
